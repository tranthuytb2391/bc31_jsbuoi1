/** 
Input:
   số có 2 chữ số

Các bước xử lý:
-Tạo biến lưu giá trị so
-tạo biên lưu giá trị soDvi,soChuc,Tong
-soDvi = so%10
-soChuc = so/10
-Tong = soDvi + soChuc

Output:
    Tổng 2 ký số = ?
*/

var so;
var soDvi;
var soChuc;
var Tong;

so = 86;

soChuc = Math.floor(so / 10);

//  Math.floor: làm tròn số

soDvi = so % 10;
Tong = soChuc + soDvi;
console.log("Tong", Tong);