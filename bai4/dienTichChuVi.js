// Bài 1: tính diện tích, chu vi HCN
/*
Input: cho biết chiều dài, chiều rông

Các bước xử lý: 

Sử dụng công thức : 

Diện tích = dài * rộng;
Chu vi = (dài +rộng) *2

Output: Diện tích =?
Chu Vi =?

 */

var chieuDai;
var chieuRong;
var dienTich = null;
var chuVi = null;

chieuDai = 7;
chieuRong = 8;

dienTich = chieuDai * chieuRong;
chuVi = (chieuDai + chieuRong) * 2;

console.log("dienTich", dienTich);
console.log("chuVi", chuVi);